##################################################################
"""Function definition"""
##################################################################
from pathlib import Path

from skimage import io
import numpy as np

taux_eau = 0.15
taux_nuage = 10
masksize = 2
current_path = Path(__file__).resolve()
path_saveTC = f"{current_path.joinpath('../data/saveTC')}"
path_data = f"{current_path.joinpath('../data')}"


def traitementneige(dataiff, band1, band2):
    # make operation (band1-band2) / (band1+band2) in a new table
    # It return the image of NDSI treatment
    im = np.zeros((dataiff.shape[0], dataiff.shape[1]))
    # variable initialisation
    for x in range(dataiff.shape[0]):
        for y in range(dataiff.shape[1]):

            a = float(dataiff[x, y, band1]) - float(dataiff[x, y, band2])
            b = float(dataiff[x, y, band1]) + float(dataiff[x, y, band2])
            if b != 0:
                im[x, y] = float(a / b)
            else:
                im[x, y] = float(a)
            # build of the image
    return im


def traitementeau(dataiff, band1, band2):
    # make operation (band1-band2) / (band1+band2) in a new table
    # It return a B&W image of where is supposed to be water at liquid state
    im = np.zeros((dataiff.shape[0], dataiff.shape[1]))
    # variable initialisation
    for x in range(dataiff.shape[0]):
        for y in range(dataiff.shape[1]):

            a = float(dataiff[x, y, band1]) - float(dataiff[x, y, band2])
            b = float(dataiff[x, y, band1]) + float(dataiff[x, y, band2])
            if b != 0:
                im[x, y] = float(a / b)
                if im[x, y] > taux_eau:
                    im[x, y] = 1
                else:
                    im[x, y] = 0
            else:
                im[x, y] = 0
            # build of the image
    return im


def clouds(Data):
    """old function with a bad tresholding"""
    sl = 0.27
    im = seuil(Data[:, :, 2], sl, 1)
    return im


def clouds2(data):
    """
    the new optimised one to detect cloud.
    it returns a tresholded image in B&W
    """
    im = seuil(data, 0.45, 0.65)
    return im


def seuil(table, taux_min, taux_max):
    """make a sceilling based on % of the variation"""
    M = table[0, 0]
    m = table[0, 0]

    im = np.zeros((table.shape[0], table.shape[1]))

    X = table.shape[0]
    Y = table.shape[1]
    M = table.max()
    m = table.min()

    seuil_min = taux_min * abs(M - m)
    seuil_max = taux_max * abs(M - m)

    for x in range(X):
        for y in range(Y):
            if table[x, y] > m + seuil_min and table[x, y] < m + seuil_max:
                im[x, y] = 1
            else:
                im[x, y] = 0

    return im


def compter_pourcentage(image):
    """to basically count the percentage of white pixel in a B&W image"""
    compte = 0
    (n, p) = np.shape(image)
    total = n * p

    for i in range(n):
        for j in range(p):
            if image[i, j] == 1:
                compte += 1

    return (compte / total) * 100


def deletewater(table, eau):
    """
    This function is removing water detected as snow from the tresholded image
    of snow based on the water position image
    """
    assert table.shape == eau.shape

    X = table.shape[0]
    Y = table.shape[1]
    for x in range(X):
        for y in range(Y):
            if eau[x, y] == 1:
                for i in range(-masksize, masksize + 1, 1):
                    for j in range(-masksize, masksize + 1, 1):
                        if (
                            x + i < X
                            and y + j < Y
                            and x + i >= 0
                            and y + j >= 0
                        ):
                            table[x + i, y + j] = 0


def addsnowontc(TC, Snow, RGB):
    # Highlight the table gived in  snow on the true color image
    assert TC.shape[0] == Snow.shape[0] and TC.shape[1] == Snow.shape[1]
    X = TC.shape[0]
    Y = TC.shape[1]
    for x in range(X):
        for y in range(Y):
            if Snow[x, y] == 1:
                TC[x, y, 0] = RGB[0]
                TC[x, y, 1] = RGB[1]
                TC[x, y, 2] = RGB[2]


def select_path():
    cpath = False
    while cpath != True:
        try:
            path = input("\nsource path : ")
            d = io.imread(path_data + path + ".tiff")
            cpath = True
        except:
            warning()
            path = ""
            cpath = False
    return path_data + path + ".tiff", path_data + "TC_" + path + ".tiff", path


def select_option():
    c = 0

    while c != 1 and c != 2 and c != 3:
        try:
            c = int(input("\n1: water \n2: snow \n3: cloud \n\nChoice : "))
        except:
            warning()

        if c != 1 and c != 2 and c != 3:
            warning()
    return c


def warning():
    print("\nthe entry you enter is incorect or not compatible \ntry again")


# both function below never get used but they serve to save snow and cloud
# coverage of an area in a txt file
def save_data(Date, S, C):
    with open("SandCcover.txt", "a") as Data:
        for i in range(len(S)):
            Data.write(str(Date[i]) + "_" + str(S[i]) + "_" + str(C[i]) + "\n")


def load_data(path):
    Date = []
    Sc = []
    Cc = []
    with open(path, "r") as Data:
        for ligne in Data:
            x = ligne[:-1].split("_")
            Date.append(x[0])
            Sc.append(int(x[1]))
            Cc.append(int(x[2]))
    return Date, Sc, Cc
